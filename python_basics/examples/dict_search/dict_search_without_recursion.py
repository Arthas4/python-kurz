"""
 Search for a key in Dictionary
"""
d = {
    'key1': 'v1',
    'key2': {
        'key21': 'v21',
        'key22': 'v22',
        'key23': 'v23',
        'key_to_find': 7,
    },
    'key3': 3,
    'key4': {
        'key41': {
            'key411': 1,
            'key_to_find': 8,
        }
    },
    'key5': {
        'key51': {
            'key511': 1,
            'key_to_find': {
                'key5111': 11,
                'key_to_find': 9,
            },
        }
    }
}


find_key = 'key_to_find'

"""
    1. Solution using 'for' and appending to the traversed list inside the loop
"""
found_key_vals = []
still_has_dict_in_vals = True
vals_to_go_into = [d]
# while len(vals_to_go_into):
for val_into in vals_to_go_into:
    for key, value in val_into.items():
        if key == find_key:
            found_key_vals.append((key, value))
        if isinstance(value, dict):
            vals_to_go_into.append(value)
print(found_key_vals)

"""
    2. Using 'while' (+ 'break') and appending and deleting to and from the list
"""
found_key_vals = []
to_go_into = [d]
while True:  # we could use a 'break' instead (this is here so we can compare it with solution 2.)
    for key, value in to_go_into[0].items():
        if key == find_key:
            found_key_vals.append((key, value))
        if isinstance(value, dict):
            to_go_into.append(value)
    del to_go_into[0]
    if len(to_go_into) == 0:
        break
print(found_key_vals)


"""
    3. Transforming the data structure to more sufficient one
"""

just_key_val_pairs = []
still_has_some_dict_inside = True
to_go_into = [d]

while still_has_some_dict_inside:  # we could use a 'break' instead (this is here so we can compare it with solution 2.)
    for key, value in to_go_into[0].items():
        just_key_val_pairs.append((key, value))
        if isinstance(value, dict):
            to_go_into.append(value)
    del to_go_into[0]
    still_has_some_dict_inside = len(to_go_into) > 0

print(list(filter(lambda x: x[0] == find_key, just_key_val_pairs)))





#
