"""
 Search for a key in Dictionary
"""
d = {
    'key1': 'v1',
    'key2': {
        'key21': 'v21',
        'key22': 'v22',
        'key23': 'v23',
        'key_to_find': 7,
    },
    'key3': 3,
    'key4': {
        'key41': {
            'key411': 1,
            'key_to_find': 8,
        }
    },
    'key5': {
        'key51': {
            'key511': 1,
            'key_to_find': {
                'key5111': 11,
                'key_to_find': 9,
            },
        }
    }
}


find_key = 'key_to_find'


def find_key_in_dict(d, find_key, depth_current=0, depth_max=None):
    found_key_vals = []
    if depth_max and (depth_current > depth_max):
        return None
    for key, value in d.items():
        if key == find_key:
            found_key_vals.append((key, value))
        if isinstance(value, dict):
            deeper_dicts_result = find_key_in_dict(
                    value,
                    find_key,
                    depth_current=depth_current + 1,
                    depth_max=depth_max
                )

            # Filter out None values (None is returned if recursion reaches the max_depth level)
            # deeper_dicts_result = list(filter(lambda i: i is not None, deeper_dicts_result))

            if deeper_dicts_result:
                found_key_vals.append(deeper_dicts_result)

    return found_key_vals


find_key_in_dict(d, find_key, depth_max=None)
find_key_in_dict(d, find_key, depth_max=5)
find_key_in_dict(d, find_key, depth_max=1)
find_key_in_dict(d, find_key, depth_max=2)


















#
