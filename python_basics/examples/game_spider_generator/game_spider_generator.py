
data = [
    # ("Name", age, condition_rate_from_1_to_5(5 is best) )
    # NOTE: here could be good to use collections.namedtuple
    ("Adam Maurenc",	12,	3,),
    ("Adam Rexa",	9,	3,),
    ("Adéla Lepová",	16,	4,),
    ("Adélka Němcová",	13,	4,),
    ("Alex Temiak",	12,	3,),
    ("Alexandra Havelková",	11,	2,),
    ("Anastázie Opatrná",	10,	3,),
    ("Aneta Hrabánková",	13,	3,),
    ("Anita Doležalová",	14,	3,),
    ("Anna Kurzová",	11,	3,),
    ("Anna Sedláková",	16,	4,),
    ("Barbora Šulcová",	12,	5,),
    ("Cecilie Dvořáková",	12,	3,),
    ("Daniel Baldík",	10,	1,),
    ("David Šulc",	15,	5,),
    ("Dominik Kundrát",	16,	4,),
    ("Elen Boráková",	12,	4,),
    ("Eliška Chválová",	13,	4,),
    ("Eliška Nová",	13,	3,),
    ("Eliška Přikrylová",	10,	3,),
    ("Emma Kalkušová",	10,	4,),
    ("Hubert Ottenschlager",	15,	5,),
    ("Jakub Kubín",	11,	3,),
    ("Jakub Veverka",	10,	3,),
    ("Jan Němec",	7,	3,),
    ("Jaroslav Kubín",	16,	4,),
    ("Jiří Marčík",	15,	5,),
    ("Jolanta Soukupová",	13,	2,),
    ("Jonáš Dařílek",	15,	4,),
    ("Julie Chválová",	10,	3,),
    ("Klaudie Hanušová",	12,	4,),
    ("Kristián Kuzdas",	13,	5,),
    ("Kristýna Cirhanová",	14,	3,),
    ("Kristýna Kolbanová",	9,	3,),
    ("Lenka Minaříková",  7,	2,),
    ("Lenka Novotná",	16,	4,),
    ("Linda Nováčková",	6,	2,),
    ("Marie Kurzová",	7,	2,),
    ("Matyáš Rexa",	12,	3,),
    ("Maxim Nováček",	12,	2,),
    ("Nella Mariee Swarzová",	11,	2,),
    ("Nikola Maurencová",	16,	3,),
    ("Radovan Kuzdas",	10,	4,),
    ("Sandra Kuzdasová",	11,	3,),
    ("Sandra Soukupová",	16,	2,),
    ("Simona Kundrátová",	12,	3,),
    ("Světlana Petra Kušnírová",	10,	4,),
    ("Tereza Chaloupková",	9,	3,),
    ("Terezie Dvořáková",	14,	3,),
    ("Tomáš Malát",	10,	3,),
    ("Václav Klouda",	8,	3,),
    ("Viktorie Hanušová",	16,	5,),
    ("Vojtěch Pipek",	12,	4),
]


# TODO: try to simplify this function
# NOTE: The function can be probably simplified using itertools.accumulate function
def accummulate_sort_weights(data, key_func):
    """
    This function calculates "weights of sort"
    Eg.: accummulate_sort_weights(['a', 'b', 'c', 'a', 'c'] -> [1, 2, 3, 1, 3]
    :param data: list of data to calculate sort-weghts for
    :param key_func: function to return sort-key for every item in the list of data
    :returns: List corresponding to sort-weight of the items of the provided data
    """
    sorted_data_keys = sorted([key_func(row) for row in data])

    # make the sort-weghts for the sorted keys of a data
    weights_of_sorted_keys = []
    prev_value = sorted_data_keys[0]
    prev_weight = 1
    for vek in sorted_data_keys:
        if vek == prev_value:
            weights_of_sorted_keys.append(prev_weight)
        else:
            prev_weight += 1
            prev_value = vek
            weights_of_sorted_keys.append(prev_weight)

    # attach the weights to values of the key
    value_weight_mapping = dict(zip(sorted_data_keys, weights_of_sorted_keys))

    # make the list of weights corresponding to the keys in the data

    return [value_weight_mapping[key_func(row)] for row in data]


print(accummulate_sort_weights(data, lambda row: row[1]))


# TODO: This function sorts the data by key and then take for each group from the beginnig to the end of the
#       sorted list. But we would like to take one from "beginning and once from end" for each group till the
#       middle of a list is reached.
#       Also we need to implement dealing with a case when length of a data is not divided be count of groups
#       without reminder
def make_uniform_groups(data, key_func, count_of_groups):
    """
    The function takes the list of data, function to return a key in the data and count of desired groups.
    Based on the key, function returns the data uniformly distributed based on key.
    :param data: list of data to be distributed
    :param key_func: function to return sort-key for every item in the list of data
    :param count_of_groups: count of groups to distribute the data into
    :return: list of distributed groups
    """
    data_sorted = sorted(data, key=key_func)
    distrib_groups = [[] for i in range(count_of_groups)]

    count_of_cycles = len(data) // count_of_groups
    for i in range(count_of_cycles):
        for j in range(count_of_groups):
            distrib_groups[j].append(data_sorted[(i*count_of_groups)+j])

    return distrib_groups


print(make_uniform_groups(data, lambda row: row[1], 4))

# TODO:
# Create data like:("name", age, condition_rank, age_sort_weight, cond_rank_sort_weight, age_plus_cond_rank_sort_weight)
# make uniform groups based on age_plus_cond_rank_sort_weight)







#
